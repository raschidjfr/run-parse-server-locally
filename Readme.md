# Running Parse Server Locally #

## Dependencies ##

* `mongodb-runner`
* `parse-server`
* `parse-dashboard`

## Installation ##

`npm install`

## Run ##

`npm start` or `npm start -- --appId <app-id> --masterKey <master-key>`.

Type `npm start` in local folder. This will 1) stop/start `mongodb-runner`, 2) start `parse-server` and 3) start `parse dashboard`.

Default configuration used:

```
appId: app
masterKey: master
server mount: http://localhost:1337/parse
dashboard url: http://localhost:4040/
```

## See Also ##

* [Parse Server](https://github.com/parse-community/parse-server)
* [Parse Dashboard](https://github.com/parse-community/parse-dashboard/)