
// Start parse server and dashboard.
const args = getArguments();
restartMongoAndParse(args.appId, args.masterKey);


function startMongoRunner(appId, masterKey) {
	const { spawn } = require('child_process');
	const command = `mongodb-runner.cmd`
	const args = ['start'];

	console.log(command, args.join(' '));
	
	let child = spawn(command, args);

	child.stdout.on('data', (data) => {
		console.log(`-MongoDB Runner-: ${data}`);
	});

	child.stderr.on('data', (data) => {
		console.log(`-MongoDB Runner-: ${data}`);
	});

	child.on('close', (code) => {
		console.log("-MongoDB Runner- exit: %s", code);
		startParseServer(appId, masterKey);
	});
}

function restartMongoAndParse(appId, masterKey) {
	const { spawn } = require('child_process');
	const command = `mongodb-runner.cmd`
	const args = ['stop'];

	console.log(command, args.join(' '));
	let child = spawn(command, args);

	child.stdout.on('data', (data) => {
		console.log(`-MongoDB Runner-: ${data}`);
	});

	child.stderr.on('data', (data) => {
		console.log(`-MongoDB Runner-: ${data}`);
	});

	child.on('close', (code) => {
		console.log("-MongoDB Runner- exit: %s", code);
		startMongoRunner(appId, masterKey);
	});
}

function stopMongo(appId, masterKey) {
	const { spawn } = require('child_process');
	const command = `mongodb-runner.cmd`
	const args = ['stop'];

	console.log(command, args.join(' '));
	let child = spawn(command, args);

	child.stdout.on('data', (data) => {
		console.log(`-MongoDB Runner-: ${data}`);
	});

	child.stderr.on('data', (data) => {
		console.log(`-MongoDB Runner-: ${data}`);
	});

	child.on('close', (code) => {
		console.log("-MongoDB Runner- exit: %s", code);
	});
}

/**
 * Spawn a process for parse dashboard with default options
 * @param {string} appId 
 * @param {string} masterKey 
 */
function startParseDashboard(appId, masterKey) {

	const { spawn } = require('child_process');
	const command = `parse-dashboard.cmd`
	const args = `--appId ${appId} --masterKey ${masterKey} --serverURL http://localhost:1337/parse --appName MyApp`.split(' ');

	console.log(command, args.join(' '));
	let child = spawn(command, args);

	child.stdout.on('data', (data) => {
		console.log(`-Parse Dashboard-: ${data}`);
	});

	child.stderr.on('data', (data) => {
		console.log(`-Parse Dashboard-: ${data}`);
	});

	child.on('close', (code) => {
		console.log("-Parse Dashboard- exit: %s", code);
	});
}

/**
 * Spawn a process for parse server with default options
 * @param {string} appId 
 * @param {string} masterKey 
 */
function startParseServer(appId, masterKey) {

	const { spawn } = require('child_process');
	const command = `parse-server.cmd`;
	const args = `--appId ${appId} --masterKey ${masterKey} --databaseURI mongodb://localhost/test`.split(' ');

	console.log(command, args.join(' '));
	let child = spawn(command, args);

	child.stdout.on('data', (data) => {
		console.log(`-Parse Server-: ${data}`);
	});

	child.stderr.on('data', (data) => {
		console.log(`-Parse Server-: ${data}`);
	});

	child.on('close', (code) => {
		console.log("-Parse Server- exit: %s", code);
		stopMongo();
	});

	startParseDashboard(appId, masterKey);
}

/**
 * Returns command line arguments from flags
 * @returns {{appId: string, masterKey: string}} 
 */
function getArguments() {

	// Get command line arguments
	const flags = require('flags');
	flags.defineString('appId', 'app', 'App Id. Default="app"');
	flags.defineString('masterKey', 'master', 'Master Key. Default="master"');
	flags.parse();

	// let directory = process.argv[2] ? process.argv[2] : flags.get('path');
	const appId = flags.get('appId');
	const masterKey = flags.get('masterKey');


	return {
		appId: appId,
		masterKey: masterKey
	}
}